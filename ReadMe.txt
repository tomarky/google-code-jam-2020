Google Code Jam 2020に参加したときのコード

Qualification Round	2020/04/04-2020/04/05 75pt 1625位 (21:07:14)
  Vestigium                      7pt
  Nesting Depth                  5pt+11pt
  Parenting Partnering Returns   7pt+12pt
  ESAb ATAd                      1pt+9pt+16pt
  Indicium                       7pt+0pt
https://codingcompetitions.withgoogle.com/codejam/round/000000000019fd27
https://codingcompetitions.withgoogle.com/codejam/submissions/000000000019fd27/VG9tYXJreQ


Round 1B 2020/04/20 28pt 3198位 (1:56:46)
  Expogo                         5pt+8pt+0pt
  Blindfolded Bullseye           3pt+12pt+0pt
  Join the Ranks                 no read
https://codingcompetitions.withgoogle.com/codejam/round/000000000019fef2
https://codingcompetitions.withgoogle.com/codejam/submissions/000000000019fef2/VG9tYXJreQ


Round 1C 2020/05/02 31pt 5206位 (1:56:48)
  Overexcited Fan                4pt+6pt+12pt
  Overrandomized                 9pt+0pt+0pt
  Oversized Pancake Choppers     0pt+0pt+0pt
https://codingcompetitions.withgoogle.com/codejam/round/000000000019fef4
https://codingcompetitions.withgoogle.com/codejam/submissions/000000000019fef4/VG9tYXJreQ


